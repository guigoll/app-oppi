class Api::V1::CategoryController < ApplicationController
  def index
    category = Category.all
    render json: { category: category }, status: 200
  end

  def show
    category = Category.find(params[:id])
    render json: category, status: 200
  end

  def create
    category = Category.create(category_params)
    if category.save
      render json: category, status: 201
    else
      render json: { errors: category }, status: 422
    end
  end

  def update
    category = Category.find(params[:id])

    if category.update_attributes(category_params)
      render json: category, status: 200
    else
      render json: { errors: category }, status: 422
    end
  end

  private

  def category_params
    params.require(:category).permit(:description)
  end
end
