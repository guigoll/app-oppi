class Api::V1::CityController < ApplicationController
  def index
    city = City.all
    render json: { city: city }, status: 200
  end

  def show
    city = City.find(params[:id])
    render json: city, status: 200
  end
end
