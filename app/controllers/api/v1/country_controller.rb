class Api::V1::CountryController < ApplicationController
  def index
    country = Country.all
    render json: { country: country }, status: 200
  end

  def show
    country = Country.find(params[:id])
    render json: country, status: 200
  end
end
