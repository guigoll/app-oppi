class Api::V1::StateController < ApplicationController
  def index
    state = State.all
    render json: { state: state }, status: 200
  end

  def show
    state = State.find(params[:id])
    render json: state, status: 200
  end
end
