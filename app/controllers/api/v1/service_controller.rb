class Api::V1::ServiceController < ApplicationController
  def index
    service = Service.all
    render json: { service: service }, status: 200
  end

  def show
    service = Service.find(params[:id])
    render json: service, status: 200
  end

  def create
    service = Service.create(service_params)

    if service.save
      render json: service, status: 201
    else
      render json: { errors: service }, status: 422
    end
  end

  def update
    service = Service.find(params[:id])

    if service.update_attributes(service_params)
      render json: service, status: 200
    else
      render json: { errors: service }, status: 422
    end
  end

  def destroy
    service = Service.find(params[:id])

    service.destroy
    render json: service, status: 200
  end

  private

  def service_params
    params.require(:service).permit(:description, :user_id, :category_id,
                                    service_items_attributes: [:description, :value, :service_id])
  end
end
