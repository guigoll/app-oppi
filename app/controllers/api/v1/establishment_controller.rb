class Api::V1::EstablishmentController < ApplicationController
  def index
    establishment = Establishment.all
    render json: { establishment: establishment }, status: 200
  end

  def show
    establishment = Establishment.find(params[:id])
    render json: establishment, status: 200
  end

  def create
    establishment = Establishment.create(establishment_params)
    if establishment.save
      render json: establishment, status: 201
    else
      render json: { errors: establishment }, status: 422
    end
  end

  def update
    establishment = Establishment.find(params[:id])

    if establishment.update_attributes(establishment_params)
      render json: establishment, status: 200
    else
      render json: { errors: establishment.errors }, status: 422
    end
  end

  def destroy
    establishment = Establishment.find(params[:id])
    establishment.destroy
    render json: establishment, status: 200
  end

  private

  def establishment_params
    params.require(:establishment).permit(:description, :qrcode, :city_id, :street, :number)
  end
end
