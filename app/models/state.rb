class State < ApplicationRecord
  belongs_to :country

  validates_presence_of :description
end
