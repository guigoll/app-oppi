class Service < ApplicationRecord
  belongs_to :user
  belongs_to :category

  has_many :service_items

  validates_presence_of :description

  accepts_nested_attributes_for :service_items, allow_destroy: true
end
