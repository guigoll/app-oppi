class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
    :recoverable, :rememberable, :trackable, :validatable

  include DeviseTokenAuth::Concerns::User

  validates_uniqueness_of :auth_token

  validates_presence_of :name, :email

  #Sempre quando for criado um usuario executa o metodo generate_authentication_token!
  before_create :generate_authentication_token!

  def generate_authentication_token!
    #Repete o bloco  caso existir algum usuario com o mesmo token informado
    begin
      self.auth_token = Devise.friendly_token
    end while User.exists?(auth_token: auth_token)
  end
end
