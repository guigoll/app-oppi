class Establishment < ApplicationRecord
  belongs_to :city

  validates_presence_of :description, :qrcode, :street, :number
end
