class ServiceItem < ApplicationRecord
  belongs_to :service, optional: true
end
