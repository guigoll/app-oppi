class CreateEstablishments < ActiveRecord::Migration[5.2]
  def change
    create_table :establishments do |t|
      t.string :description
      t.string :qrcode

      t.timestamps
    end
  end
end
