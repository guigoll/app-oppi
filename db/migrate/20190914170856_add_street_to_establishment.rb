class AddStreetToEstablishment < ActiveRecord::Migration[5.2]
  def change
    add_column :establishments, :street, :string
  end
end
