class CreateEstablishmentUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :establishment_users do |t|
      t.references :establishment, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
