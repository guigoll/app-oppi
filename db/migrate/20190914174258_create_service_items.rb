class CreateServiceItems < ActiveRecord::Migration[5.2]
  def change
    create_table :service_items do |t|
      t.string :description
      t.decimal :value
      t.references :service, foreign_key: true

      t.timestamps
    end
  end
end
