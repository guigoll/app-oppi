require "api_version_constraint"

Rails.application.routes.draw do
  devise_for :users, only: [:sessions], controller: { sessions: "api/v1/sessions" }

  namespace :api, default: { format: :json }, path: "/" do
    namespace :v1, path: "/", constraints: ApiVersionConstraint.new(version: 1, default: true) do
      mount_devise_token_auth_for "User", at: "auth"

      resources :users, only: [:index, :show, :create]
      resources :category, only: [:index, :show, :create, :update]
      resources :kind, only: [:index, :show, :create, :update]
      resources :service, only: [:index, :show, :create, :update, :destroy]
      resources :establishment, only: [:index, :show, :create, :update, :destroy]
      resources :country, only: [:index, :show]
      resources :state, only: [:index, :show]
      resources :city, only: [:index, :show]
    end
  end
end
