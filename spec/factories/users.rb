FactoryBot.define do
  factory :user do
    name { Faker::Name.name }
    nickname { Faker::Twitter.screen_name}
    email { Faker::Internet.free_email }
    password { Faker::Internet.password }
    image {Faker::File.file_name(dir: 'path/to')}
  end
end
