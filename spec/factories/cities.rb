FactoryBot.define do
  factory :city do
    description { Faker::Address.city }
    state
  end
end
