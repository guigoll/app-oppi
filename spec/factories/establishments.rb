FactoryBot.define do
  factory :establishment do
    description { Faker::Company.name }
    qrcode { Faker::Company.brazilian_company_number }
    street { Faker::Address.street_name }
    number { Faker::Address.building_number }
    city
  end
end
