FactoryBot.define do
  factory :country do
    description { Faker::Address.country }
  end
end
