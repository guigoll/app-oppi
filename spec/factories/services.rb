FactoryBot.define do
  factory :service do
    description { Faker::Commerce.department }
    category
    user

    trait :with_service_item do
      after(:create) do |service|
        create(:service_item, service: service)
      end
    end
  end
end
