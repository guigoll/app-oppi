FactoryBot.define do
  factory :state do
    description { Faker::Address.state }
    country
  end
end
