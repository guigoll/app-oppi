FactoryBot.define do
  factory :kind do
    description { Faker::Commerce.department }
  end
end
