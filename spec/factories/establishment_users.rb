FactoryBot.define do
  factory :establishment_user do
    establishment
    user
  end
end
