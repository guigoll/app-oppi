FactoryBot.define do
  factory :service_item do
    description { Faker::Lorem.words }
    value { Faker::Commerce.price }
    service
  end
end
