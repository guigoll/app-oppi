FactoryBot.define do
  factory :category do
    description { Faker::Company.industry }
  end
end
