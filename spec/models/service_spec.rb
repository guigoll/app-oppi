require "rails_helper"

RSpec.describe Service, type: :model do
  let(:service) { FactoryBot.build(:service) }

  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:category) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to respond_to(:description) }
  it { is_expected.to respond_to(:user_id) }
  it { is_expected.to respond_to(:category_id) }
end
