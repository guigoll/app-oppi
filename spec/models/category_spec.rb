require "rails_helper"

RSpec.describe Category, type: :model do
  let(:category) { FactoryBot.build(:category) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to have_many(:services) }

  it { is_expected.to respond_to(:description) }
end
