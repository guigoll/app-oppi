require "rails_helper"

RSpec.describe Country, type: :model do
  let(:country) { FactoryBot.build(:country) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to respond_to(:description) }
end
