require "rails_helper"

RSpec.describe Kind, type: :model do
  let(:kind) { FactoryBot.build(:kind) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to respond_to(:description) }
end
