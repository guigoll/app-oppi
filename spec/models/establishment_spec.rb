require "rails_helper"

RSpec.describe Establishment, type: :model do
  let(:establishment) { FactoryBot.build(:establishment) }

  it { is_expected.to belong_to(:city) }

  it { is_expected.to validate_presence_of :description }
  it { is_expected.to validate_presence_of :qrcode }
  it { is_expected.to validate_presence_of :street }
  it { is_expected.to validate_presence_of :number }

  it { is_expected.to respond_to(:description) }
  it { is_expected.to respond_to(:qrcode) }
  it { is_expected.to respond_to(:city_id) }
  it { is_expected.to respond_to(:street) }
  it { is_expected.to respond_to(:number) }
end
