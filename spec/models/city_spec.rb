require "rails_helper"

RSpec.describe City, type: :model do
  let(:city) { FactoryBot.build(:city) }

  it { is_expected.to belong_to(:state) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to respond_to(:description) }
  it { is_expected.to respond_to(:state_id) }
end
