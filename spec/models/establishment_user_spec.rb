require "rails_helper"

RSpec.describe EstablishmentUser, type: :model do
  let(:establisment_user) { FactoryBot.create(:establisment_user) }

  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:establishment) }

  it { is_expected.to respond_to(:user_id) }
  it { is_expected.to respond_to(:establishment_id) }
end
