require "rails_helper"

RSpec.describe State, type: :model do
  let(:state) { FactoryBot.create(:state) }

  it { is_expected.to belong_to(:country) }

  it { is_expected.to validate_presence_of :description }

  it { is_expected.to respond_to(:description) }
  it { is_expected.to respond_to(:country_id) }
end
