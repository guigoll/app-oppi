require "rails_helper"

RSpec.describe "Kind API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:kind) { Factory.create(:kind) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],

    }
  end

  describe "GET /kind" do
    before do
      create_list(:kind, 5)
      get "/kind", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 kind of database" do
      expect(json_body[:kind].count).to eq(5)
    end
  end

  describe "GET /kind/:id" do
    let(:kind) { FactoryBot.create(:kind) }

    before { get "/kind/#{kind.id}", params: {}, headers: headers }

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return json of the kind" do
      expect(json_body[:description]).to eq(kind.description)
    end
  end

  describe "POST /kind" do
    before do
      post "/kind", params: { kind: kind_params }.to_json, headers: headers
    end

    context "When the params are valid" do
      let(:kind_params) { FactoryBot.build(:kind) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "save kind in database" do
        expect(Kind.find_by(description: kind_params[:description])).not_to be_nil
      end

      it "return the json, when create in database" do
        expect(json_body[:description]).to eq(kind_params[:description])
      end
    end

    context "When the params are invalid" do
      let(:kind_params) { attributes_for(:kind, description: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "when no save in database" do
        expect(Kind.find_by(description: kind_params[:description])).to be_nil
      end

      it "return json of error" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "PUT /kind/:id" do
    let(:kind) { FactoryBot.create(:kind) }

    before do
      put "/kind/#{kind.id}", params: { kind: kind_params }.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:kind_params) { { description: "Teste atualizacao0" } }

      it "return status code 200" do
        expect(response).to have_http_status(200)
      end

      it "return json updated" do
        expect(json_body[:description]).to eq(kind_params[:description])
      end

      it "updated kind in database" do
        expect(Kind.find_by(description: kind_params[:description])).not_to be_nil
      end
    end

    context "when the params are invalid" do
      let(:kind_params) { { description: "" } }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "not updated kind in database" do
        expect(Kind.find_by(description: kind_params[:description])).to be_nil
      end

      it "return json with errors" do
        expect(json_body).to have_key(:errors)
      end
    end
  end
end
