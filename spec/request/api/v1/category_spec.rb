require "rails_helper"

RSpec.describe "Category API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:category) { FactoryBot.create(:category) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],
    }
  end

  describe "GET /category" do
    before do
      create_list(:category, 5)
      get "/category", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 category in the database" do
      expect(json_body[:category].count).to eq(5)
    end
  end

  describe "POST /category" do
    before do
      post "/category", params: { category: category_params }.to_json, headers: headers
    end

    context "When the params are valid" do
      let(:category_params) { FactoryBot.create(:category) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "save category in database" do
        expect(Category.find_by(description: category_params[:description])).not_to be_nil
      end

      it "return json when create category" do
        expect(json_body[:description]).to eq(category_params[:description])
      end
    end

    context "when the params are invalid" do
      let(:category_params) { attributes_for(:category, description: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "when not save in database" do
        expect(Category.find_by(description: category_params[:description])).to be_nil
      end

      it "retorn json with errors" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "UPDATE /category" do
    let!(:category) { FactoryBot.create(:category) }
    before do
      put "/category/#{category.id}", params: { category: category_params }.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:category_params) { { description: "Test of update" } }

      it "return status code 200" do
        expect(response).to have_http_status(200)
      end

      it "return json of database" do
        expect(json_body[:description]).to eq(category_params[:description])
      end

      it "updated category in database" do
        expect(Category.find_by(description: category_params[:description])).not_to be_nil
      end
    end

    context "when the params are invalid" do
      let(:category_params) { { description: "" } }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "no updated in database" do
        expect(Category.find_by(description: category_params[:description])).to be_nil
      end

      it "return json error" do
        expect(json_body).to have_key(:errors)
      end
    end
  end
end
