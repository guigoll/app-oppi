require "rails_helper"

RSpec.describe "Service API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:service) { FactoryBot.create(:service) }
  let(:category) { FactoryBot.create(:category) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.opi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],

    }
  end

  describe "GET /service" do
    before do
      create_list(:service, 5)
      get "/service", params: {}, headers: headers
    end

    it "return stus code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 service" do
      expect(json_body[:service].count).to eq(5)
    end
  end

  describe "GET /service/:id" do
    let(:service) { FactoryBot.create(:service) }

    before { get "/service/#{service.id}", params: {}, headers: headers }

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return json in database" do
      expect(json_body[:description]).to eq(service.description)
    end
  end

  describe "POST /service" do
    before do
      post "/service", params: { service: service_params }.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:service_params) { FactoryBot.create(:service, user_id: user.id, category_id: category.id) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "return service save in database" do
        expect(Service.find_by(description: service_params[:description])).not_to be_nil
      end

      it "return json of service create in database" do
        expect(json_body[:description]).to eq(service_params[:description])
      end
    end

    context "when the params are invalid" do
      let(:service_params) { attributes_for(:service, description: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "service not save in database" do
        expect(Service.find_by(description: service_params[:description])).to be_nil
      end

      it "return json error" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "PUT /service/:id" do
    let(:service) { FactoryBot.create(:service) }

    before do
      put "/service/#{service.id}", params: { service: service_params }.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:service_params) { { description: "Teste de descricao" } }

      it "return status code 200" do
        expect(response).to have_http_status(200)
      end

      it "return json updated" do
        expect(json_body[:description]).to eq(service_params[:description])
      end

      it "update service in database" do
        expect(Service.find_by(description: service_params[:description])).not_to be_nil
      end
    end

    context "when the params are invalid" do
      let(:service_params) { { description: "" } }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "not update service in database" do
        expect(Service.find_by(description: service_params[:description])).to be_nil
      end

      it "return json error of service" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "DELETE /service/:id" do
    let!(:service) { FactoryBot.create(:service) }

    before do
      delete "/service/#{service.id}", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "remove service in databse" do
      expect(Service.find_by(description: service.description)).to be_nil
    end
  end
end
