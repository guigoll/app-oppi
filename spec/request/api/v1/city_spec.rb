require "rails_helper"

RSpec.describe "City API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:city) { FactoryBot.create(:city) }
  let(:state) { FactoryBot.create(:state) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],
    }
  end

  describe "GET /city" do
    before do
      create_list(:city, 5)
      get "/city", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 city create in database" do
      expect(json_body[:city].count).to eq(5)
    end
  end

  describe "GET /city/:id" do
    let(:city) { FactoryBot.create(:city) }

    before { get "/city/#{city.id}", params: {}, headers: headers }

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return json city" do
      expect(json_body[:description]).to eq(city.description)
    end
  end
end
