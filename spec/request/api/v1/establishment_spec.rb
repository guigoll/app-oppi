require "rails_helper"

RSpec.describe "Establishment API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }
  let(:city) { FactoryBot.create(:city) }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],

    }
  end

  describe "GET /establishment" do
    before do
      create_list(:establishment, 5)
      get "/establishment", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 establishment" do
      expect(json_body[:establishment].count).to eq(5)
    end
  end

  describe "GET /establishment/:id" do
    let(:establishment) { FactoryBot.create(:establishment) }

    before { get "/establishment/#{establishment.id}", params: {}, headers: headers }

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return json establishment" do
      expect(json_body[:description]).to eq(establishment.description)
    end
  end

  describe "POST /establishment" do
    before do
      post "/establishment", params: { establishment: establishment_params }.to_json, headers: headers
    end

    context "when the description are valid" do
      let(:establishment_params) { FactoryBot.build(:establishment, city_id: city.id) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "save the establishment in database" do
        expect(Establishment.find_by(description: establishment_params["description"])).not_to be_nil
      end

      it "return json when create estabblishment" do
        expect(json_body[:description]).to eq(establishment_params[:description])
      end
    end

    context "when the  description is invalid" do
      let(:establishment_params) { attributes_for(:establishment, description: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "when the establishment not save in database" do
        expect(Establishment.find_by(description: establishment_params[:description])).to be_nil
      end

      it "return json error" do
        expect(json_body).to have_key(:errors)
      end
    end

    context "when the qrcode is valid" do
      let(:establishment_params) { FactoryBot.build(:establishment, city_id: city.id) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "save the establishment in database" do
        expect(Establishment.find_by(qrcode: establishment_params["qrcode"])).not_to be_nil
      end

      it "return json when create estabblishment" do
        expect(json_body[:qrcode]).to eq(establishment_params[:qrcode])
      end
    end

    context "when the qrcode is invalid" do
      let(:establishment_params) { attributes_for(:establishment, qrcode: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "when the establishment not save in database" do
        expect(Establishment.find_by(qrcode: establishment_params[:qrcode])).to be_nil
      end

      it "return json error" do
        expect(json_body).to have_key(:errors)
      end
    end

    context "when the street is valid" do
      let(:establishment_params) { FactoryBot.build(:establishment, city_id: city.id) }

      it "return status code 201" do
        expect(response).to have_http_status(201)
      end

      it "save the establishment in database" do
        expect(Establishment.find_by(street: establishment_params["street"])).not_to be_nil
      end

      it "return json when create estabblishment" do
        expect(json_body[:street]).to eq(establishment_params[:street])
      end
    end

    context "when the street is invalid" do
      let(:establishment_params) { attributes_for(:establishment, street: "") }

      it "return status code 422" do
        expect(response).to have_http_status(422)
      end

      it "when the establishment not save in database" do
        expect(Establishment.find_by(street: establishment_params[:street])).to be_nil
      end

      it "return json error" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "UPDATE /establishment/:id" do
    let!(:establishment) { FactoryBot.create(:establishment) }

    before do
      put "/establishment/#{establishment.id}", params: { establishment: establishment_params }.to_json, headers: headers
    end

    context "when the params are valid" do
      let(:establishment_params) { { description: "test establishment" } }

      it "return status code 200" do
        expect(response).to have_http_status(200)
      end

      it "when establishment not save in database" do
        expect(Establishment.find_by(description: establishment_params[:description])).not_to be_nil
      end

      it "return json establishment" do
        expect(json_body[:description]).to eq(establishment_params[:description])
      end
    end

    context "when the params are invalid" do
      let(:establishment_params) { { description: "" } }

      it "when the params are invalid" do
        expect(response).to have_http_status(422)
      end

      it "when establishment not save in database" do
        expect(Establishment.find_by(description: establishment_params[:description])).to be_nil
      end

      it "return json erro" do
        expect(json_body).to have_key(:errors)
      end
    end
  end

  describe "DELETE /establidhment/:id" do
    let(:establishment) { FactoryBot.create(:establishment) }

    before do
      delete "/establishment/#{establishment.id}", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "delete establishment in database" do
      expect(Establishment.find_by(id: establishment.id)).to be_nil
    end
  end
end
