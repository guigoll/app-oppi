require "rails_helper"

RSpec.describe "State API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:state) { FactoryBot.create(:state) }
  let(:country) { FactoryBot.create(:country) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],
    }
  end

  describe "GET /State" do
    before do
      create_list(:state, 5)
      get "/state", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 state in database" do
      expect(json_body[:state].count).to eq(5)
    end
  end

  describe 'GET /State/:id' do
    let(:state){ FactoryBot.create(:state)}

    before{ get "/state/#{state.id}", params: {}, headers: headers}

    it 'return status code 200' do
        expect(response).to have_http_status(200)
    end
   
    it 'return json in state' do
        expect(json_body[:description]).to eq(state.description)
    end

  end

end
