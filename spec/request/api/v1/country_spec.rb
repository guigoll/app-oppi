require "rails_helper"

RSpec.describe "Country API", type: :request do
  before { host! "api.oppi.com.br" }

  let(:country) { FactoryBot.create(:country) }
  let(:user) { FactoryBot.create(:user) }
  let(:auth_data) { user.create_new_auth_token }

  let(:headers) do
    {
      "Accept" => "application/vnd.oppi.v1",
      "Content-type" => Mime[:json].to_s,
      "Access-token" => auth_data["access-token"],
      "uid" => auth_data["uid"],
      "client" => auth_data["client"],
    }
  end

  describe "GET /Country" do
    before do
      create_list(:country, 5)
      get "/country", params: {}, headers: headers
    end

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "return 5 coutry in database" do
      expect(json_body[:country].count).to eq(5)
    end
  end

  describe "GET /Country/:id" do
    let(:country) { FactoryBot.create(:country) }

    before { get "/country/#{country.id}", params: {}, headers: headers }

    it "return status code 200" do
      expect(response).to have_http_status(200)
    end

    it "does something" do
      expect(json_body[:description]).to eq(country.description)
    end
  end
end
